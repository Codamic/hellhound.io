---
layout: default
hero: true
warning: true
---
### What is HellHound ?
**HellHound** is an open source set of libraries to create simple and elegant programs based on streams. An **HellHound** application
basically is a [system](http://docs.hellhound.io/guides/#_systems) of [components](http://docs.hellhound.io/guides/#_components)
which work together in order to achieve a common goal. Components form one or more data pipelines through
[workflows](http://docs.hellhound.io/guides/#_workflow_2). In general systems are a great way to manage the lifecycle and data flow
or your program and components are awesome for managing the state and dependencies of different pieces of your program.

HellHound provides different built-in components for different types of systems. For example Webserver component for creating
a fullstack web application, or a kafka component for a data processing application. For more information checkout the
[guides](http://docs.hellhound.io/guides/) and [Examples](https://github.com/Codamic/hellhound_examples).

---
### Announcements

##### We have moved to Gitlab
As you already know, Microsoft aquired Github. We are not happy about this and we can't trust Microsoft at all. They already
showed their true nature to the world ([Halloween documents](https://en.wikipedia.org/wiki/Halloween_documents)). So we decided
to move to [Gitlab](//gitlab.com) as an alternative.

---

### Where to get help ?
* [#hellhound](http://webchat.freenode.net/?channels=hellhound&uio=d4) IRC channel on [freenode](https://freenode.net/
)
* #hellhound channel on [clojurians](http://clojurians.net/)
